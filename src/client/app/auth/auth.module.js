(function() {
    'use strict';
    angular
        .module('auth', [])
        .constant("API", {
            "url": "http://localhost:3001",
        });
})();