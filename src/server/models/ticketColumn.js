import mongoose from 'mongoose';

const TicketColumn = new mongoose.Schema({
    columnTitle: {
        type: String,
        required: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    type: {
        type: String,
        default: 'column'
    },
    is_deleted: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('TicketColumn', TicketColumn);