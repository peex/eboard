(function() {
    'use strict';
    angular.module('eboard.core', [
        /*
         * Angular modules
         */
        'ngResource',
        'ngCookies',
        /* Cross-app modules */
        'angularModalService',
        'dndLists'
        /* 3rd-party modules */
    ]);
})();