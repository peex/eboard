(function() {
    'use strict';
    angular
        .module('auth')
        .component('loginPage', {
            templateUrl: 'app/auth/login/loginPage.html',
            controller: loginPage
        });

    loginPage.$inject = ['$http', '$location', '$cookieStore', 'API'];
    function loginPage($http, $location, $cookieStore, API) {
        var self = this;
        self.loginError = '';
        self.fieldRequired = true;
        self.login = login;

        function login() {
            if(self.loginForm.$valid){
                $http.post(API.url+'/login', {
                    email: self.loginForm.email.$modelValue,
                    password: self.loginForm.password.$modelValue,
                }, {withCredentials: true}).success(function(response){
                    if(response){
                        $cookieStore.put('currentUser', response);
                        $location.path("/dashboard");
                    }

                }).error(function(response){
                    self.loginError = response.message ? response.message : 'Something went wrong';
                });
            }
        }
    }
})();