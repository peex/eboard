module.exports = {
    server: {
        port: 3001
    },
    database: {
        master: {
            host: "mongodb://localhost/",
            database: "eboard"
        }
    }
};