import User from '../models/user';
import bcrypt from 'bcrypt'
const registerUser = {
    create: async (userData) => {
        let newTicketColumn = new User({
            email: userData.email,
            password: bcrypt.hashSync(userData.password, 10)
        });
        return await newTicketColumn.save();
    },
};
export default registerUser;