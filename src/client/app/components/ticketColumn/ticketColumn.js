(function() {
    'use strict';
    angular
        .module('dashboard')
        .component('ticketColumn', {
            bindings: {
                columnTitle: '<',
                columnIndex: '<',
                removeColumn: '<',
                columnId: '<'
            },
            templateUrl: 'app/components/ticketColumn/ticketColumn.html',
            controller: ticketColumn
        });
    ticketColumn.$inject = ['$http','ModalService', 'API'];

    function ticketColumn($http, ModalService, API) {
            var self = this;
            this.newColumnTitle = this.columnTitle;
            this.columnTickets = [];
            this.models = {
                selected: null
            };

            self.addTicket = addTicket;
            self.removeTicket = removeTicket;
            self.switchTicketImportant = switchTicketImportant;
            self.editColumnTitle = editColumnTitle;
            self.initColumn = initColumn;

            // Initialization
            (function initialization() {
                initColumn();
            })();

            function initColumn() {
                if(self.columnId){
                    $http.get(API.url+'/ticket/all/'+self.columnId, {withCredentials: true}).success(function(response){
                        self.columnTickets = response;
                    });
                }
            }
            function addTicket() {
                ModalService.showModal({
                    templateUrl: "app/components/modal/addTicket.html",
                    controller: function(){
                        var scope = this;
                        this.formData = {};
                        this.add = function(){
                            var postData = scope.formData;
                            postData['column'] = self.columnId;
                            $http.post(API.url+'/ticket', postData, {withCredentials: true}).success(function(response){
                                self.columnTickets.push(response);
                                scope.element.modal('hide');
                            });
                        }
                    },
                    controllerAs: 'addTicketCtrl'
                }).then(function(modal){
                    modal.controller.element = modal.element;
                    modal.element.modal();
                });
            }
            function editColumnTitle() {
                ModalService.showModal({
                    templateUrl: "app/components/modal/editColumnTitle.html",
                    controller: function(){
                        var scope = this;
                        scope.newColumnTitle = self.columnTitle;
                        scope.updateColumnTitle = function(){
                            $http.put(API.url+'/ticketColumn/'+self.columnId, {columnTitle: scope.newColumnTitle}, {withCredentials: true}).success(function(response){
                                self.columnTitle = scope.newColumnTitle;
                                scope.element.modal('hide');
                            });
                        }
                    },
                    controllerAs: 'editColumnTitleCtrl'
                }).then(function(modal){
                    modal.controller.element = modal.element;
                    modal.element.modal();
                });
            }
            function removeTicket(index) {
                ModalService.showModal({
                    templateUrl: "app/components/modal/confirmRemoveTicket.html",
                    controller: function(){
                        var scope = this;
                        scope.text = 'Remove ticket '+self.columnTickets[index].emailTitle +'('+self.columnTickets[index].emailAddr+')';
                        scope.confirm = function(){
                            $http.delete(API.url+'/ticket/'+self.columnTickets[index]._id, {withCredentials: true}).success(function(response){
                                self.columnTickets.splice(index, 1);
                            });
                        }
                    },
                    controllerAs: 'removeTicketCtrl'
                }).then(function(modal){
                    modal.element.modal();
                });
            }
            function switchTicketImportant(index) {
                $http.put(API.url+'/ticket/'+self.columnTickets[index]._id, {isImportant: !self.columnTickets[index].isImportant}, {withCredentials: true}).success(function(response){
                    self.columnTickets[index].isImportant = !self.columnTickets[index].isImportant;
                });
            }
        }
})();