(function() {
    'use strict';
    angular
        .module('dashboard')
        .component('ticketInfo',  {
            bindings: {ticketId: '<'},
            templateUrl: 'app/components/ticket/ticketInfo.html',
            controller: ticketInfo
        });

    ticketInfo.$inject = ['$state', 'ModalService', '$http', 'API'];

    function ticketInfo($state, ModalService, $http, API) {
        var self = this;

        self.ticketInfo = {};
        self.availableColumns = {};

        self.saveTicketInfo = saveTicketInfo;
        self.moveTicketToColumn = moveTicketToColumn;
        self.shareCandidate = shareCandidate;
        self.sendEmail = sendEmail;
        self.addToList = addToList;
        self.archiveTicket = archiveTicket;
        self.displayModal = displayModal;

        self.initTicket = initTicket;
        self.initColumns = initColumns;

        // Initialization
        (function initialization() {
            initTicket();
            initColumns();
        })();

        function initTicket() {
            $http.get(API.url+'/ticket/'+$state.params.ticketId, {withCredentials: true}).success(function(response){
                self.ticketInfo = response;
            });
        }
        function initColumns() {
            $http.get(API.url+'/ticketColumn/', {withCredentials: true}).success(function(response){
                self.availableColumns = response;
            });
        }
        function saveTicketInfo() {
            if(self.ticketInfoForm.$valid){
                var data = self.ticketInfo;
                //delete data['_id'];
                $http.put(API.url+'/ticket/'+self.ticketInfo._id, data, {withCredentials: true}).success(function(response){
                    console.log(response, 'OK!!!');
                });
            }else{
                console.log('ERRROR!!!');
            }
        }
        function moveTicketToColumn(columnId) {
            if(self.ticketInfoForm.$valid){
                $http.put(API.url+'/ticket/'+self.ticketInfo._id, {column: columnId}, {withCredentials: true}).success(function(response){
                    console.log(response, 'OK!!!');
                });
            }else{
                console.log('ERRROR!!!');
            }
        }

        function shareCandidate() {
            self.displayModal(
                'app/components/modal/ticket/shareCandidate.html',
                'shareCandidateCtrl',
                function () {
                    var scope = this;
                    scope.candidateName = self.ticketInfo.emailAddr;
                    scope.hrName = '';
                    scope.share = function () {
                        scope.element.modal('hide');
                    }

                }
            )
        }
        function sendEmail() {
            self.displayModal(
                'app/components/modal/ticket/sendEmail.html',
                'sendEmailCtrl',
                function () {
                    var scope = this;
                    scope.candidateName = self.ticketInfo.emailAddr;
                    scope.emailText = '';
                    scope.send = function () {
                        console.log('I AM HERE!!!!', scope.emailText);
                        scope.element.modal('hide');
                    }

                }
            )
        }
        function addToList(listType) {
            self.displayModal(
                'app/components/modal/ticket/addToList.html',
                'addToListCtrl',
                function () {
                    var scope = this;
                    scope.candidateName = self.ticketInfo.emailAddr;
                    scope.listType = listType;
                    scope.add = function () {
                        var propertyTitle = (scope.listType == 'black' ? 'isInBlackList' : 'isInTalentList');
                        var sendData = {};
                        sendData[propertyTitle] = true;
                        $http.put(API.url+'/ticket/'+self.ticketInfo._id, sendData, {withCredentials: true}).success(function(response){
                            console.log('I AM HERE!!!!', scope.listType);
                            scope.element.modal('hide');
                        });
                    }

                }
            )
        }
        function archiveTicket() {
            self.displayModal(
                'app/components/modal/ticket/archiveTicket.html',
                'archiveTicketCtrl',
                function () {
                    var scope = this;
                    scope.candidateName = self.ticketInfo.emailAddr;
                    scope.archive = function () {
                        $http.delete(API.url+'/ticket/'+self.ticketInfo._id, {withCredentials: true}).success(function(response){
                            console.log('I AM HERE!!!!', scope.listType);
                            scope.element.modal('hide');
                        });
                    }

                }
            )
        }
        function displayModal(templateUrl, ctrlAs, ctrl) {
            ModalService.showModal({
                templateUrl: templateUrl,
                controller: ctrl,
                controllerAs: ctrlAs
            }).then(function(modal){
                modal.controller.element = modal.element;
                modal.element.modal();
            });
        }
    }
})();