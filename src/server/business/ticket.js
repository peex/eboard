import Ticket from '../models/ticket';

const ticketActions = {
    getAll: async (columnId) => {
        return await Ticket.find({is_deleted: false, column: columnId});
    },
    getOne: async (id) => {
        return await Ticket.findOne({_id: id});
    },
    create: async (ticketData, ownerId) => {
        ticketData['owner'] = ownerId;
        let newTicket = new Ticket(ticketData);
        return await newTicket.save();
    },
    update: async (id, ticketData) => {
        return await Ticket.findOneAndUpdate({_id: id}, ticketData);
    },
    delete: async (id) => {
        return await Ticket.findOneAndUpdate({_id: id}, {is_deleted: true});
    }
};
export default ticketActions;