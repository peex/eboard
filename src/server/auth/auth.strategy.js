import passport from 'koa-passport';
import User from '../models/user';
import bcrypt from 'bcrypt';

passport.serializeUser(function(user, done) {
    done(null, user._id)
});

passport.deserializeUser(async function(id, done) {
    User.findById(id, done);
});

const LocalStrategy = require('passport-local').Strategy
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},function(email, password, done) {
    User.findOne({ email: email }, '_id email password', (err, result) => {
        if(err){
            done({message: 'Mongo query Error!'})
        }
        if(!result){
            done({message: 'User Not Found'})
        }else{
            bcrypt.compare(password, result.password).then((passwordMatch) => {
                if(passwordMatch){
                    done(null, {_id: result._id, email: result.email});
                } else {
                    done({message: 'Password is wrong'});
                }
            });
        }

    });
}));