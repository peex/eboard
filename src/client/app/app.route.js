(function() {
    'use strict';
    angular
        .module('eboard')
        .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", function($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;

        $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
        $httpProvider.defaults.headers.common['Access-Control-Allow-Credentials'] = 'true';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $urlRouterProvider.otherwise('/login');
        var authCheck = {
            security: ['$cookieStore', '$state', function ($cookieStore, $state) {
                var currentUser = $cookieStore.get('currentUser');
                if(!currentUser || !currentUser.hasOwnProperty('_id')){
                    $state.go('login');
                }
            }]
        };
        var alreadyAuthCheck = {
            security: ['$cookieStore', '$state', function ($cookieStore, $state) {
                var currentUser = $cookieStore.get('currentUser');
                if(currentUser && currentUser.hasOwnProperty('_id')){
                    $state.go('dashboard');
                }
            }]
        };
        var dashBoardState = {
            url: '/dashboard',
            templateUrl: "app/dashboard/dashboard.html",
            controller: "dashboardController",
            resolve: authCheck
        };
        var ticketInfoState = {
            url: '/ticket/{ticketId}',
            template: '<ticket-info></ticket-info>',
            resolve: authCheck
        };
        var registrationPageState = {
            url: '/registration',
            template: '<registration-page></registration-page>',
            resolve: alreadyAuthCheck
        };
        var loginPageState = {
            url: '/login',
            template: '<login-page></login-page>',
            resolve: alreadyAuthCheck
        };

        $stateProvider.state('dashboard', dashBoardState);
        $stateProvider.state('ticketInfo', ticketInfoState);
        $stateProvider.state('registration', registrationPageState);
        $stateProvider.state('login', loginPageState);
    }]);

})();