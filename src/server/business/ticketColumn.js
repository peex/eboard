import TicketColumn from '../models/ticketColumn';
const ticketColumnActions = {
    getAll: async (ownerId) => {
        return await TicketColumn.find({is_deleted: false, owner: ownerId});
    },
    create: async (columnData, ownerId) => {
        columnData['owner'] = ownerId;
        let newTicketColumn = new TicketColumn(columnData);
        return await newTicketColumn.save();
    },
    update: async (id, columnData, ownerId) => {
        return await TicketColumn.findOneAndUpdate({_id: id, owner: ownerId}, columnData);
    },
    delete: async (id, ownerId) => {
        return await TicketColumn.findOneAndUpdate({_id: id, owner: ownerId}, {is_deleted: true});
    }
};
export default ticketColumnActions;