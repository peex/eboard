import Koa from 'koa';
import config from  'config';
import mongoose from 'mongoose';
import err from './middleware/error';
import cors from'koa-cors';
import {routes, allowedMethods} from './middleware/routes';
// Mongoose init code.
mongoose.connect('mongodb://localhost/eboard');
mongoose.connection.on('error', console.error);

//const passport = require('koa-passport')
const app = new Koa();

// Sessions
const convert = require('koa-convert') ;// necessary until koa-generic-session has been updated to support koa@2
const session = require('koa-generic-session');
app.keys = ['secret'];
app.use(convert(session()));

require('../server/auth/auth.strategy');
const passport = require('koa-passport');
app.use(passport.initialize());
app.use(passport.session());

app.use(err);
app.use(cors({credentials: true}));
app.use(routes());
app.use(allowedMethods());

app.listen(config.server.port, function () {
    console.log('%s listening at port %d', config.server.port);
});