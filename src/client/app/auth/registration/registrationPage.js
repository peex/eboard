(function() {
    'use strict';
    angular
        .module('auth')
        .component('registrationPage', {
            templateUrl: 'app/auth/registration/registrationPage.html',
            controller: registrationPage
        });

    registrationPage.$inject = ['$http', '$location', 'API', '$cookieStore'];
    function registrationPage($http, $location, API, $cookieStore) {
        var self = this;
        self.fieldRequired = true;
        self.register = register;

        function register() {
            self.duplicateEmailError = false;

            self.registrationForm.password.$setValidity('passLength', self.registrationForm.password.$modelValue.length >= 6);
            self.registrationForm.repassword.$setValidity('repassLength', self.registrationForm.repassword.$modelValue.length >= 6);

            self.registrationForm.password.$setValidity('passMatch', self.registrationForm.password.$modelValue == self.registrationForm.repassword.$modelValue);
            self.registrationForm.repassword.$setValidity('passMatch', self.registrationForm.password.$modelValue == self.registrationForm.repassword.$modelValue);

            if(self.registrationForm.$valid){
                $http.post(API.url+'/register', {
                    email: self.registrationForm.email.$modelValue,
                    password: self.registrationForm.password.$modelValue,
                }).success(function(response){
                    if(response){
                        $cookieStore.put('currentUser', response);
                        $location.path("/dashboard");
                    }
                }).error(function(response){
                    if(response.message.indexOf('duplicate key error') > -1){
                      self.duplicateEmailError = true;
                    }
                });
            }
        }
    }
})();