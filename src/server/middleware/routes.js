import Router from 'koa-router';
import convert from 'koa-convert';
import ticketColumn from '../business/ticketColumn';
import ticket from '../business/ticket';
import register from '../auth/register';
import passport from 'koa-passport';
import KoaBody from 'koa-body';

const router = new Router(),
    koaBody = convert(KoaBody());

router
    .get('/ticketColumn', async (ctx, next) => {
        ctx.body = await ticketColumn.getAll(ctx.state.user._id)
    })
    .post('/ticketColumn', koaBody, async (ctx, next) => {
        ctx.status = 201;
        ctx.body = await ticketColumn.create(ctx.request.body, ctx.state.user._id)
    })
    .put('/ticketColumn/:id', koaBody, async (ctx, next) => {
        ctx.status = 204;
        await ticketColumn.update(ctx.params.id, ctx.request.body, ctx.state.user._id);
    })
    .delete('/ticketColumn/:id', async (ctx, next) => {
        ctx.status = 204;
        await ticketColumn.delete(ctx.params.id, ctx.state.user._id);
    })
    .post('/register', koaBody, async (ctx, next) => {
        ctx.status = 201;
        await register.create(ctx.request.body);
    })
    .post('/login', koaBody, passport.authenticate('local'), async (ctx, next) => {
        ctx.status = 200;
        ctx.body = ctx.state.user;
    })
    .get('/logout', async (ctx, next) => {
        ctx.logout();
    })
    .get('/ticket/all/:columnId', async (ctx, next) => {
        ctx.body = await ticket.getAll(ctx.params.columnId);
    })
    .get('/ticket/:ticketId', async (ctx, next) => {
        ctx.body = await ticket.getOne(ctx.params.ticketId);
    })
    .post('/ticket', koaBody, async (ctx, next) => {
        ctx.status = 201;
        ctx.body = await ticket.create(ctx.request.body, ctx.state.user._id)
    })
    .put('/ticket/:ticketId', koaBody, async (ctx, next) => {
        ctx.status = 204;
        await ticket.update(ctx.params.ticketId, ctx.request.body);
    })
    .delete('/ticket/:ticketId', async (ctx, next) => {
        ctx.status = 204;
        await ticket.delete(ctx.params.ticketId);
    });


export function routes () { return router.routes() }
export function allowedMethods () { return router.allowedMethods() }