(function() {
    'use strict';
    angular
        .module('dashboard', [])
        .constant("API", {
            "url": "http://localhost:3001",
        });
})();