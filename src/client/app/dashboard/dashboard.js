(function() {
    'use strict';
    angular
        .module('dashboard')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$scope', '$http', 'ModalService', 'API'];

    function dashboardController($scope, $http, ModalService, API) {
        $scope.ticketColumns = [
            {columnTitle: 'Title 1', type: 'column'},
            {columnTitle: 'Title 2', type: 'column'},
            {columnTitle: 'Title 3', type: 'column'}
        ];
        $scope.models = {
            selected: null
        };
        
        $scope.initDashboard = initDashboard;
        $scope.addNewColumn = addNewColumn;
        $scope.removeColumn = removeColumn;
        // Initialization
        (function initialization() {
            initDashboard();
        })();

        function initDashboard() {
            $http.get(API.url+'/ticketColumn', {withCredentials: true}).success(function(response){
                $scope.ticketColumns = response;
            });
        }
        function addNewColumn() {
            var currentTicketsCount = $scope.ticketColumns.length + 1;
            var newColumnData = {columnTitle: 'New Column '+currentTicketsCount};
            $http.post(API.url+'/ticketColumn', newColumnData, {withCredentials: true}).success(function(response){
                initDashboard();
            });

        }
        function removeColumn(index) {
            ModalService.showModal({
                templateUrl: "app/components/modal/confirmRemoveColumn.html",
                controller: function(){
                    var scope = this;
                    scope.column = $scope.ticketColumns[index];
                    scope.text = 'Remove Column '+scope.column.columnTitle;
                    scope.confirm = function(){
                        $http.delete(API.url+'/ticketColumn/'+scope.column._id, {withCredentials: true}).success(function(response){
                            $scope.ticketColumns.splice(index, 1);
                        });
                    }
                },
                controllerAs: 'removeColumnCtrl'
            }).then(function(modal){
                modal.element.modal();
            });
        }

    }
})();