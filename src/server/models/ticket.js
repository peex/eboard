import mongoose from 'mongoose';

const Ticket = new mongoose.Schema({
    emailTitle: {
        type: String,
        required: true
    },
    emailAddr: {
        type: String,
        required: true
    },
    emailTags: {
        type: String,
    },
    column: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TicketColumn',
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    isImportant: {
        type: Boolean,
        default: false
    },
    isInBlackList: {
        type: Boolean,
        default: false
    },
    isInTalentList: {
        type: Boolean,
        default: false
    },
    attachments: [],
    expectedSalary: {
        type: String,
    },
    startDate: {
        type: String,
    },
    addedDate: {
        type: Date,
        default: Date.now
    },
    contacts: {
        type: String,
    },
    description: {
        type: String,
    },
    specialMarks: {
        type: String,
    },
    is_deleted: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('Ticket', Ticket);